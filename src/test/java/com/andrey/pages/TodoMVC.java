package com.andrey.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.WebDriverRunner.url;

/**
 * Created by aalis_000 on 26.03.2016.
 */
public class TodoMVC {

    public static ElementsCollection tasks = $$("#todo-list li");

    public static SelenideElement newTask = $("#new-todo");

    @Step
    public static void add(String... taskTexts) {
        for (String text : taskTexts) {
            newTask.setValue(text).pressEnter();
        }
    }

    @Step
    public static SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).$(".edit").setValue(newTaskText);
    }

    @Step
    public static void delete(String taskTexts) {
        tasks.find(exactText(taskTexts)).hover().$(".destroy").click();
    }

    @Step
    public static void toogle(String taskTexts) {
        tasks.find(exactText(taskTexts)).$(".toggle").click();
    }

    @Step
    public static void toogleAll() {
        $("#toggle-all").click();
    }

    @Step
    public static void clearCompleted() {
        $("#clear-completed").click();
        $("#clear-completed").shouldBe(hidden);
    }

    @Step
    public static void filterActive() {
        $(By.linkText("Active")).click();
    }

    @Step
    public static void filterAll() {
        $(By.linkText("All")).click();
    }

    @Step
    public static void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    @Step
    public static void assertTasks(String... taskTexts) {
        tasks.filter(visible).shouldHave(exactTexts(taskTexts));
    }

    @Step
    public static void assertNoTasks() {
        tasks.filter(visible).shouldBe(empty);
    }

    @Step
    public static void assertItemsLeft(int count) {
        $("#todo-count strong").shouldHave(exactText(Integer.toString(count)));
    }


    // helpers

    public  enum Filter {
        ALL(""), ACTIVE("active"), COMPLETED("completed");

        public  String url;

        Filter(String url) {
            this.url = "https://todomvc4tasj.herokuapp.com/#/" + url;
        }

        public  String getUrl() {
            return url;
        }

    }

    public static void ensurePageOpened(Filter filter) {
        if (!url().equals(filter.getUrl())) {
            open(filter.getUrl());
        }
    }

    @Step
    public static void givenHelper(Filter filter, Task... tasks) {
        ensurePageOpened(filter);

        String JScript = "localStorage.setItem('todos-troopjs', '[";
        for (Task task : tasks) {

            JScript += "{\"completed\":" + task.taskType.getFlag() + ", \"title\":\"" + task.taskText + "\"},";
        }

        if (tasks.length > 0) {
            JScript = JScript.substring(0, (JScript.length() - 1));
        }

        executeJavaScript(JScript + "]');");
        executeJavaScript("location.reload()");
    }


    public  enum TaskType {
        ACTIVE("false"), COMPLETED("true");

        public  String flag;

        TaskType(String flag) {
            this.flag = flag;
        }

        public  String getFlag() {
            return flag;
        }
    }

    public static class Task {
        String taskText;
        TaskType taskType;

        public  Task(TaskType taskType, String taskText) {
            this.taskText = taskText;
            this.taskType = taskType;
        }

    }

    public static Task aTask(TaskType taskType, String taskText) {
        return new Task(taskType, taskText);
    }

    @Step
    public static Task[] getTasks(TaskType taskType, String... taskTexts) {
        Task[] tasks = new Task[taskTexts.length];
        for (int i = 0; i < taskTexts.length; i++) {
            tasks[i] = new Task(taskType, taskTexts[i]);
        }
        return tasks;
    }

    @Step
    public static void givenTasksAtAll(TaskType taskType, String... taskText) {
        givenHelper(Filter.ALL, getTasks(taskType, taskText));
    }

    @Step
    public static void givenTasksAtActive(TaskType taskType, String... taskText) {
        givenHelper(Filter.ACTIVE, getTasks(taskType, taskText));
    }

    @Step
    public static void givenTasksAtCompleted(TaskType taskType, String... taskText) {
        givenHelper(Filter.COMPLETED, getTasks(taskType, taskText));
    }

    @Step
    public static void givenTasksAtAll(Task... tasks) {
        givenHelper(Filter.ALL, tasks);
    }

    @Step
    public static void givenTasksAtActive(Task... tasks) {
        givenHelper(Filter.ACTIVE, tasks);
    }

    @Step
    public static void givenTasksAtCompleted(Task... tasks) {
        givenHelper(Filter.COMPLETED, tasks);
    }


}
