package com.andrey;

import com.andrey.categories.Buggy;
import com.andrey.categories.Smoke;
import com.andrey.pages.BaseTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.andrey.pages.TodoMVC.TaskType.ACTIVE;
import static com.andrey.pages.TodoMVC.TaskType.COMPLETED;
import static com.andrey.pages.TodoMVC.*;

/**
 * Created by aalis_000 on 26.03.2016.
 */
public class TodosOperationsAtAllFilterTest extends BaseTest {

    @Test
    @Category(Smoke.class)
    public void deleteAtAll() {
        givenTasksAtAll(ACTIVE, "1", "2");

        delete("1");
        assertTasks("2");
        assertItemsLeft(1);
    }

    @Test
    @Category(Smoke.class)
    public void reopenAtAll() {
        givenTasksAtAll(aTask(COMPLETED, "1"),
                aTask(ACTIVE, "2"));

        toogle("1");
        assertTasks("1", "2");
        assertItemsLeft(2);
    }

    @Test
    @Category(Buggy.class)
    public void completeAllAtAll() {
        givenTasksAtAll(ACTIVE, "1", "2");  //// BUG!!!

        toogleAll();
        assertTasks("2");
        assertItemsLeft(0);
    }

    @Test
    @Category(Smoke.class)
    public void reopenAllAtAll() {
        givenTasksAtAll(COMPLETED, "1");

        toogleAll();
        assertTasks("1");
        assertItemsLeft(1);
    }

    @Test
    public void cancelEditClickingEscapeAtAll() {
        givenTasksAtAll(aTask(ACTIVE, "1"),
                aTask(COMPLETED, "2"));

        startEdit("1", "1 cancel edit").pressEscape();
        assertTasks("1", "2");
        assertItemsLeft(1);
    }

    @Test
    public void deleteByClearingTextAtAll() {
        givenTasksAtAll(ACTIVE, "1");

        startEdit("1", "").pressEnter();
        assertNoTasks();
    }

    @Test
    public void confirmEditByClickingOutsideAtAll() {
        givenTasksAtAll(ACTIVE, "1");

        startEdit("1", "1 edited");
        newTask.click();
        assertItemsLeft(1);
        assertTasks("1 edited");
    }

    @Test
    public void switchFromAllToActive() {
        givenTasksAtAll(COMPLETED, "1");

        filterActive();
        assertNoTasks();
    }
}
